import React from 'react';
import { Time } from '../../components';
import './style.scss';

export default class WidgetBarLeft extends React.Component {
  render() {
    return (
      <div id="widget-bar-left" className="widget-bar-left">
        <Time />
      </div>
    );
  }
}