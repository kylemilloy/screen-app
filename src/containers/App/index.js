import React from 'react';
import WidgetBarLeft from '../WidgetBarLeft';
import WidgetBarRight from '../WidgetBarRight';
import { Background } from '../../components';
import './style';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <Background />
        <WidgetBarLeft />
        <WidgetBarRight />
      </div>
    );
  }
}