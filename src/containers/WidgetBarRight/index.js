import React from 'react';
import { Weather, Calendar } from '../../components';
import './style.scss';

export default class WidgetBarRight extends React.Component {
  render() {
    return (
      <div id="widget-bar-right" className="widget-bar-right">
        <Weather />
        <Calendar />
      </div>
    );
  }
}