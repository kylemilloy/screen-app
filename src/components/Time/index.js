import React from 'react';
import moment from 'moment';

import './style.scss';

export default class Time extends React.Component {
  constructor( props ) {
    super( props );
    this.timeInterval;
    this.state = {
      date: new Date()
    };
  }
  
  componentDidMount() {
    const timeout = 1000 * 1; // 1sec
    this.timeInterval = setInterval( () => {
      this.setState({
        date: new Date()
      });
    }, timeout );
  }
  
  componentWillUnmount() {
    clearInterval( this.timeInterval );
    this.timeInterval = null;
  }
  
  getHour() {
    let time = moment( this.state.date ).format( 'hh' );
    return time;
  }
  getMinute() {
    let time = moment( this.state.date ).format( 'mm' );
    return time;
  }
  getAmPm() {
    let time = moment( this.state.date ).format( 'a' );
    return time;
  }
  
  getHandPosition( hand ) {
    if ( typeof hand !== "string" ) {
      console.error( "hand position must be sent as string with either hour/minute/second as a value" )
      return false;
    }
    
    var fraction = 1 / 1;
    let date = this.state.date;
    let degrees = 360;
    hand = hand.toLowerCase();
    
    switch( hand ) {
      case "hour":
        fraction = date.getHours() / 12
        break;
      case "minute":
        fraction = date.getMinutes() / 60
        break;
      case "second":
        fraction = date.getSeconds() / 60
        break;
      default:
        console.error( "hand position must be sent as string with either hour/minute/second as a value" )
        return false;
    }
    
    return fraction * degrees;
  }

  render() {
    let secondHandPos = this.getHandPosition( "second" );
    let minuteHandPos = this.getHandPosition( "minute" );
    let hourHandPos = this.getHandPosition( "hour" );
    
    return(
      <div id="time-widget">
        <p>
          { this.getHour() }:{ this.getMinute() }
        </p>
        <div className="clock">
          <div className="hour hand" style={{ transform: `rotate( ${ hourHandPos }deg )` }}></div>
          <div className="minute hand" style={{ transform: `rotate( ${ minuteHandPos }deg )` }}></div>
          <div className="second hand" style={{ transform: `rotate( ${ secondHandPos }deg )` }}></div>
        </div>
      </div>
    );
  }
}