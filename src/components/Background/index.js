import React from 'react';
import _ from 'lodash';
import $ from 'jquery';

import videos from './videos';
import './style.scss';


export default class Background extends React.Component {
  constructor( props ) {
    super( props );
    this.state = {
      video1: '',
      video2: ''
    };
  }
  
  componentDidMount() {
    let video1 = document.getElementById( 'background-video-1' );
    let video2 = document.getElementById( 'background-video-2' );
    
    this.setState({
      video1: this.getVideoSource(),
      video2: this.getVideoSource()
    });
    
    video1.ontimeupdate = () => {
      this.transitionListener( video1 );
    }
    video2.ontimeupdate = () => {
      this.transitionListener( video2 );
    }
  }
  
  componentWillUnmount() {
    let video1 = document.getElementById( 'background-video-1' );
    let video2 = document.getElementById( 'background-video-2' );
    
    video1.ontimeupdate = null;
    video2.ontimeupdate = null;
  }
  
  transitionListener( vid ) {
    if ( vid.duration - 10 <= vid.currentTime ) {
      let $vid = $( vid );
      let $alt = $vid.siblings( 'video' );
      let $parent = $vid.parent( '#background-video' );
      
      if ( $parent.data( 'transition' ) === false ) {
        $parent.data( 'transition', true );
        $vid.toggleClass( 'hide' );
        $alt.toggleClass( 'hide' );
        $alt[ 0 ].currentTime = 0;
        $alt[ 0 ].play();
        setTimeout( () => {
          let state = $vid.attr( 'id' ) === 'background-video-1' ? 'video1' : 'video2';
          $parent.data( 'transition', false );
          console.log( vid );
          this.setState({
            [ state ]: this.getVideoSource()
          });
          
          vid.pause();
        }, 1000 * 10 );
      }
    }
  }
   
  getVideoSource() {
    let video = _.sample( videos );
    return video.url;
  }
  
  render() {
    
    return(
      <div id="background-video" className="background-video" data-transition="false">
        <video id="background-video-1" src={ this.state.video1 } className="player" autoPlay></video>
        <video id="background-video-2" src={ this.state.video2 } className="player hide"></video>
      </div>
    );
  }
}