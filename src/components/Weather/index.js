import React from 'react';
import $ from 'jquery';

import './style.scss';

export default class Weather extends React.Component {
  
  constructor( props ) {
    super( props );
    this.WEATHER_API_URL = 'https://api.forecast.io/forecast';
    this.WEATHER_API_KEY = 'ea168cffd7aca4b6354e4b62da59e36f';
    this.GEO_API_URL = 'http://freegeoip.net/json/';
    this.latitude = null;
    this.longitude = null;
    this.getLatLong();
    
    this.interval;
    
    this.state = {
      currently: {
        icon: '',
        summary: '',
        temp: 0,
        wind: {
          speed: 0,
          direction: ''
        }
      }
    }
  }
  
  componentDidMount() {
    const self = this;
    this.interval = setInterval( () => {
      self.getForecast();
    }, 1000 * 60 * 60 );
  }
  componentWillUnmount() {
    clearInterval( this.interval );
    this.interval = null;
  }
  
  getLatLong() {
    $.get( this.GEO_API_URL )
      .done( function( res ) {
        this.latitude = res.latitude
        this.longitude = res.longitude
        this.getForecast();
      }.bind( this ));
  }
  
  getForecast() {
    let url = `${ this.WEATHER_API_URL }/${ this.WEATHER_API_KEY }/${ this.latitude },${ this.longitude }?units=si`;
    $.ajax({ dataType: 'jsonp', url: url })
      .done( function( res ) {
        this.setState({
          currently: {
            icon: `./assets/climacons/${ res.currently.icon }.svg`,
            summary: res.currently.summary,
            temp: Math.round( res.currently.temperature ),
            wind: {
              speed: Math.round( res.currently.windSpeed ),
              direction: this.getBearing( res.currently.windBearing )
            }
          },
          daily: res.daily.data
        })
      }.bind( this ));
  }
  
  getBearing( deg ) {
    let dir = '';
    if ( deg >= 0 && deg < 22.5 && deg >= 337.5 ) {
      dir = 'N';
    }
    else if ( deg >= 22.5 && deg < 67.5 ) {
      dir = 'NE';
    }
    else if ( deg >= 67.5 && deg < 112.5 ) {
      dir = 'E';
    }
    else if ( deg >= 112.5 && deg < 157.5 ) {
      dir = 'SE';
    }
    else if ( deg >= 157.5 && deg < 202.5 ) {
      dir = 'S'
    }
    else if ( deg >= 202.5  && deg < 247.5 ) {
      dir = 'SW';
    }
    else if ( deg >= 247.5 && deg < 292.5 ) {
      dir = 'W';
    }
    else if ( deg >= 292.5 && deg < 337.5 ) {
      dir = 'NW';
    }
    
    return dir;
  }
  
  render() {
    return (
      <div id="weather-widget">
        <div id="current-weather" className="current-weather">
          <div className="temp">
            <p>
              { this.state.currently.temp }
            </p>
          </div>
          <div className="icon">
            <img src={ this.state.currently.icon } height="100" width="100" />
          </div>
        </div>
      </div>
    );
  }
}