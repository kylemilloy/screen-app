import React from 'react';
import moment from 'moment';
import './style.scss';

export default class Calendar extends React.Component {
  constructor( props ) {
    super( props );
    this.interval = null;
    
    const date = moment();
    this.state = {
      day: date.format( 'D' ),
      month: date.format( 'MMMM' ),
      year: date.format( 'Y' )
    };
  }
  
  componentDidMount() {
    this.interval = setInterval(
      this.getDate(),
      1000 * 60
    );
  }
  
  getDate() {
    const date = moment();
    this.setState({
      day: date.format( 'D' ),
      month: date.format( 'MMMM' ),
      year: date.format( 'Y' )
    });
  }
  
  render() {
    return (
      <div id="calendar">
        <span className="month">{ this.state.month }</span>
        <span className="day">{ this.state.day }</span>
        <span className="year">{ this.state.year }</span>
      </div>
    )
  }
}