const autoprefixer = require( 'autoprefixer' );
const path = require( 'path' );
const precss = require( 'precss' );
const webpack = require( 'webpack' );

const config = {
  context: __dirname,
  entry: './src',
  output: {
    path: path.join( __dirname, 'dist' ),
    filename: '/index.js'
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)?$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: [ 'react', 'es2015', 'stage-0' ]
        }
      },
      {
        test: /\.scss$/,
        loader: 'style!css!sass'
      },
      {
        test: /\.(woff|woff2)$/,
        loader: 'url-loader?limit=10000&name=assets/fonts/[name].[ext]'
      },
      {
        test: /\.(ttf|eot|svg|otf)$/,
        loader: 'file-loader?limit=10000&name=assets/fonts/[name].[ext]'
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'file-loader'
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"development"'
    })
  ],
  postcss: function() {
    return [
      autoprefixer,
      precss
    ];
  }
};

module.exports = config;